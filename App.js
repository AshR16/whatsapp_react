import React from "react";
import { Text, StyleSheet, View } from "react-native";
//import Routes from "./Navigation/Routes";
import WhatsAppNavigator from "./src/Navigation/WhatsAppNavigator";

export default function App() {
  return <WhatsAppNavigator />;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "grey",
  },
});
