import Profile from "../Models/Profile";


export const Data = [
  new Profile('c1', 'Liam',"https://i.pinimg.com/564x/88/7f/04/887f04e372b0529e7c5d4c0034b6776f.jpg" ,'9:45 am',"Hello World","Today, 12:45 am"),
  new Profile('c2', 'Olivia', "https://i.pinimg.com/564x/88/7f/04/887f04e372b0529e7c5d4c0034b6776f.jpg", '9:45 am',"Hello World"),
  new Profile('c3', 'Noah',"https://i.pinimg.com/564x/88/7f/04/887f04e372b0529e7c5d4c0034b6776f.jpg" ,'9:45 am',"Hello World"),
  new Profile('c4', 'Emma',"https://i.pinimg.com/564x/88/7f/04/887f04e372b0529e7c5d4c0034b6776f.jpg" ,'9:45 am',"Hello World"),
  new Profile('c5', 'Ramesh',"https://i.pinimg.com/564x/88/7f/04/887f04e372b0529e7c5d4c0034b6776f.jpg" ,'9:45 am',"Hello World"),
  new Profile('c6', 'Suresh',"https://i.pinimg.com/564x/88/7f/04/887f04e372b0529e7c5d4c0034b6776f.jpg" ,'9:45 am',"Hello World"),
  new Profile('c7', 'Geetha',"https://i.pinimg.com/564x/88/7f/04/887f04e372b0529e7c5d4c0034b6776f.jpg" ,'9:45 am',"Hello World"),
  new Profile('c8', 'Jay',"https://i.pinimg.com/564x/88/7f/04/887f04e372b0529e7c5d4c0034b6776f.jpg" ,'9:45 am',"Hello World"),
  new Profile('c9', 'Elijah',"https://i.pinimg.com/564x/88/7f/04/887f04e372b0529e7c5d4c0034b6776f.jpg" ,'9:45 am',"Hello World"),
  new Profile('c10', 'Charlotte',"https://i.pinimg.com/564x/88/7f/04/887f04e372b0529e7c5d4c0034b6776f.jpg" ,'9:45 am',"Hello World")
];