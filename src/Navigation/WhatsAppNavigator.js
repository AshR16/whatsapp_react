import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TextInput,
} from "react-native";
// import { Avatar, Button, Card, Title, Paragraph } from "react-native-paper";
// import SearchScreen from "../components/SearchScreen";
import { Ionicons, Entypo } from "@expo/vector-icons";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

import { createMaterialTopTabNavigator } from "react-navigation-tabs";
import CallScreen from "../Screens/CallScreen";
import ChatScreen from "../Screens/ChatScreen";
import StatusScreen from "../Screens/StatusScreen";


const Mytabs = createMaterialTopTabNavigator(
  {
    chats: ChatScreen,
    status: StatusScreen,
    calls: CallScreen,
  },
  {
    tabBarOptions: {
      style: {
        backgroundColor: "#047a6c",
      },

      headerStyle: {
        backgroundColor: "047a6c",
      },
    },
  },
  {
    initialRouteName: "chats",
  }
);

// creates stack navigator
const myStack = createStackNavigator(
  {
    home: Mytabs,
  },
  {
    defaultNavigationOptions: {
      title: "WhatsApp",
      headerStyle: {
        backgroundColor: "#047a6c",
        elevation: 0,
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
      headerRight: () => (
        <View style={{ flexDirection: "row", padding: 20 }}>
          <Ionicons name="md-search" size={30} color="white" />
          <Entypo
            name="dots-three-vertical"
            size={23}
            color="white"
            style={{ marginLeft: 10 }}
          />
        </View>
      ),
    },
  }
);
export default createAppContainer(myStack);
