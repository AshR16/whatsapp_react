import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from "react-native";
import { Avatar, Card, Title, Paragraph, Button } from "react-native-paper";
import Mycard from "../components/MyCards";

import { Data } from "../Data/dummyData";

export default function ChatScreen(props) {
  const renderItem = (Item) => {
    return (
      <TouchableWithoutFeedback>
        <Mycard
          name={Item.item.name}
          image={Item.item.image}
          Paragraph={Item.item.Paragraph}
          time={Item.item.time}
          onPress={() => {
            console.log("On press");
            props.navigation.navigate({
              routeName: "chatLog",
            });
          }}
        />
      </TouchableWithoutFeedback>
    );
  };

  return <FlatList data={Data} renderItem={renderItem} />;
}

ChatScreen.navigationOptions = {
  headerStyle: {
    backgroundColor: "#047a6c",
  },
};
