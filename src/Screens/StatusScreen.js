import React from "react";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import { Avatar, Card, Title, Paragraph, Button } from "react-native-paper";
import Mycard from "../components/MyCards";

export default function StatusScreen(props) {
  const image =
    "https://i.pinimg.com/564x/88/7f/04/887f04e372b0529e7c5d4c0034b6776f.jpg";
  return (
    <View>
      <ScrollView>
        <Mycard
          name="My status"
          image={image}
          Paragraph="Tap to add status update"
        />
        <View style={styles.ViewedBar}>
          <Text>Recent Updates</Text>
        </View>
        <Mycard name="hitesh" image={image} Paragraph="Today, 12:45 am" />
        <Mycard name="mukesh" image={image} Paragraph="Today, 12:45 am" />
        <Mycard name="saurabh" image={image} Paragraph="Today, 12:45 am" />
        <Mycard name="lina" image={image} Paragraph="Today, 12:45 am" />
        <Mycard name="meena" image={image} Paragraph="Today, 12:45 am" />
        <Mycard name="saurabh" image={image} Paragraph="Today, 12:45 am" />
        <Mycard name="saurabh" image={image} Paragraph="Today, 12:45 am" />
        <Mycard name="saurabh" image={image} Paragraph="Today, 12:45 am" />
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  ViewedBar: {
    padding: 15,
    fontSize: 15,
  },
});
