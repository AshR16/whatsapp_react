import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { Avatar, Button, Card, Title, Paragraph } from "react-native-paper";
import { Ionicons } from "@expo/vector-icons";

export default function CallCards(props) {
  return (
    <TouchableOpacity>
      <View style={{ margin: 0 }} onPress={props.onSelect}>
        <Card style={{ elevation: 0 }}>
          <View style={styles.cardDetail}>
            <View style={{ flexDirection: "row", padding: 10 }}>
              <View>
                <Image
                  style={{ height: 80, width: 80, borderRadius: 40 }}
                  source={{
                    uri: "https://i.pinimg.com/564x/88/7f/04/887f04e372b0529e7c5d4c0034b6776f.jpg",
                  }}
                />
              </View>
              <View>
                <Card.Content>
                  <Title>Ashwini Rai</Title>
                  <Paragraph>kjdsbhkdf hfbyuer</Paragraph>
                </Card.Content>
              </View>
            </View>
            <View style={styles.callIcon}>
                            <Ionicons name="ios-call-sharp" size={30} color="#047a6c" />
                            </View>
          </View>
        </Card>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  cardDetail: {
    flexDirection: "row",
    padding: 0,
    flex:2,
  },
  callIcon:{
      flex:1,
      flexDirection:"row",
      justifyContent:"flex-end",
      alignItems:"center",
      padding:25
  }
});
