import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { Avatar, Button, Card, Title, Paragraph } from "react-native-paper";

export default function Mycard(props) {
  return (
    <TouchableOpacity>
      <View style={{ margin: 0 }} onPress={props.onSelect}>
        <Card style={{ elevation: 0 }}>
          <View style={styles.cardDetail}>
            <View style={{ flexDirection: "row", padding: 10 }}>
              <View>
                <Image
                  style={{ height: 80, width: 80, borderRadius: 40 }}
                  source={{
                    uri: props.image,
                  }}
                />
              </View>
              <View>
                <Card.Content>
                  <Title>{props.name}</Title>
                  <Paragraph>{props.Paragraph}</Paragraph>
                </Card.Content>
              </View>
            </View>
            <View style={styles.callIcon}>
              <Text>{props.time}</Text>
            </View>
          </View>
        </Card>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  cardDetail: {
    flexDirection: "row",
    padding: 0,
    flex: 2,
  },
  callIcon: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
    padding: 25,
  },
});
